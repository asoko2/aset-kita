<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/animate.min.css',
        'css/bootstrap.css',
        'css/bootstrap.min.css',
        'css/icustom.css',
        'css/font-awesome.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        'css/styl.bluee.css',
        'css/style.default.css',
        'css/style.green.css',
        'css/style.mono.css',
        'css/style.pink.css',
        'css/style.violet.css'
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/bootstrap-hover-dropdown.js',
        'js/front.js',
        'js/jquery.cookie.js',
        'js/jquery.flexslider.js',
        'js/jquery.flexslider.min.js',
        'js/jquery-1.11.0.min.js',
        'js/main.js',
        'js/modernizr.js',
        'js/owl.carousel.min.js',
        'js/waypoints.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
